# jsCafe.styledocco

## Styledocco についておさらい
<img src="../resources/fig1.png" alt="" width="300" style="float:right;">

NodeJS のパッケージの一つでCSSファイルからスタイルガイドを自動作成するスタイルシートをドキュメント化するプログラムです。

CSSファイルやプリプロセッサ用のファイル(SCSS、LESS、Stylus)に記述したMarkdownテキストからドキュメントを抽出し作成します。

今までは別途用意していたようなスタイルガイドも、CSSファイルにドキュメントとしての仕様やメモを記述できる様になります。

また、CSSファイルそのものがドキュメントになる事で、複数メンバーが管理する場合に起こるスタイルガイドとスタイルシートの乖離が少なくなる事で、生きたドキュメント作成をサポートします。


## 使い方
Styledocco パッケージのインストール

`npm install -fg styledocco`

---

スタイルガイドの作成

`styledocco stylesheets docs`

---

Styledocco のオプション

* `--name, -n [プロジェクトネーム]`  
	プロジェクトネーム
* `--out, -o [出力ディレクトリ]`  
	出力ディレクトリ (default: "docs")
* `--preprocessor [コマンド呼び出し]`  
	カスタム プリプロセッサ コマンド (optional) (ex: --preprocessor "~/bin/lessc")
* `--include [(foo.css | foo.js)]`  
	プレビューへのCSS もしくは JavaScript ファイルのインクルード (optional) (ex: --include mysite.css --include app.js)
* `--verbose`  
	ドキュメント出力時のログ表示 (default: false)


